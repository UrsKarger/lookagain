@echo off
set MY_PROJECT_NAME=replay

choice /C YN /T 3 /D N /M "change project name (default:%MY_PROJECT_NAME%)"
if ERRORLEVEL 255 goto :run_cmake_cmd
if ERRORLEVEL 2 goto :run_cmake_cmd
if ERRORLEVEL 1 goto :change_project_name
if ERRORLEVEL 0 goto :run_cmake_cmd

:change_project_name
set /p MY_PROJECT_NAME_INP="Project Name [Default %MY_PROJECT_NAME%] :"
if NOT "X%MY_PROJECT_NAME_INP%" == "X" (
    set MY_PROJECT_NAME=%MY_PROJECT_NAME_INP%
)

:run_cmake_cmd
set PRJ_ROOT=%~dp0
echo project name: %MY_PROJECT_NAME%
echo project root: %PRJ_ROOT%
py .\scripts\run_cmake.py ^
    --name %MY_PROJECT_NAME% ^
    --cfg .\scripts\cmake_cfg.ini ^
    --root %PRJ_ROOT% ^
    --delete_cache

goto :EOF

:get_absolute
set absolute=%~f1

:EOF
REM pause