/**
 * \file pluginPlayer.h
 */

#ifndef IPLUGIN_DLL_API_EXPORTING
#define IPLUGIN_DLL_API_EXPORTING
#endif

#include <IPlugin_export.h>
#include <IPlugin.h>
#include <IPluginIf.h>

class PluginPlayer final : IPlugin
{
    public:
    PluginPlayer(){};
    ~PluginPlayer(){};

    uint32_t init(const char *filename) override{};
    uint32_t run(const void *pInput, void *const pOutput) override {}
    uint32_t finalize(const void *pInput, void *const pOutput) override{};


    private:
};

extern "C" {


IPLUGIN_DLL_API uint32_t
registerPlugin(PluginIdx_t idx, char name[IPLUGIN_NAME_SIZE])
{
    return 0;
}

IPLUGIN_DLL_API uint32_t releasePlugin(PluginIdx_t idx) { return 0; }

IPLUGIN_DLL_API uint32_t registerInputPort(
    PluginIdx_t idx, size_t size, char name[IPLUGIN_PORT_NAME_SIZE])
{
    return 0;
}

IPLUGIN_DLL_API uint32_t registerOuputPort(
    PluginIdx_t idx, size_t size, char name[IPLUGIN_PORT_NAME_SIZE])
{
    return 0;
}

IPLUGIN_DLL_API uint32_t init(PluginIdx_t idx, const char *filename)
{
    return 0;
}

IPLUGIN_DLL_API uint32_t
run(PluginIdx_t idx, const void *pInput, void *const pOutput)
{
    return 0;
}

IPLUGIN_DLL_API uint32_t
finalize(PluginIdx_t idx, const void *pInput, void *const pOutput)
{
    return 0;
}
}
