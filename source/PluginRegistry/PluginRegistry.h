#ifndef PLUGINREGISTRY_H
#define PLUGINREGISTRY_H

#include <ipluginif.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <set>
#include <memory>
#include <cstring>

class PluginRegistry
{
public:
    PluginRegistry( );
    ~PluginRegistry( ) = default;

    void readConfiguration( ){};
    void loadPlugins( ){};

private:
    struct PluginHandle_t
    {
        HMODULE moduleId;
        PluginIdx_t index;
        char name[IPLUGIN_NAME_SIZE];
    };

    struct plugin_handle_less
    {
        bool operator( )(const PluginHandle_t &lhs, const PluginHandle_t &rhs) const
        {
            if (strcmp(lhs.name, rhs.name) < 0)
            {
                return true;
            }
            return false;
        }
    };

    using storageType = std::set<PluginHandle_t, plugin_handle_less>;

    std::unique_ptr<storageType> m_plugins;
};

#endif /* PLUGINREGISTRY_H */
