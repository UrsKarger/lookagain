/**
 * \file IPlugin_export.h
 */

#ifndef IPLUGIN_EXPORT_H
#define IPLUGIN_EXPORT_H

#ifdef IPLUGIN_DLL_API_EXPORTING
#define IPLUGIN_DLL_API __declspec(dllexport)
#else
#define IPLUGIN_DLL_API __declspec(dllimport)
#endif

#endif /* IPLUGIN_EXPORT_H */ s
