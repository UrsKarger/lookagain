/**
 * \file IPluginIf.h
 */

#ifndef IPLUGINIF_H
#define IPLUGINIF_H

#include <cstdint>

#include "IPlugin_export.h"

/*efine 1234567890123456789012345678901 */
#define IPLUGIN_NO_OF_CHAR_IN_NAME 31U
#define IPLUGIN_NAME_SIZE (IPLUGIN_NO_OF_CHAR_IN_NAME + 1U)

#define IPLUGIN_NO_OF_CHAR_IN_PORT 31U
#define IPLUGIN_PORT_NAME_SIZE (IPLUGIN_NO_OF_CHAR_IN_PORT + 1U)

extern "C" {
typedef uint8_t PluginIdx_t;

typedef struct PluginPort_st
{
    size_t dataSize;
    char dataName[IPLUGIN_PORT_NAME_SIZE];
} PluginPort_t;

extern IPLUGIN_DLL_API uint32_t
registerPlugin(PluginIdx_t idx, char name[IPLUGIN_NAME_SIZE]);

extern IPLUGIN_DLL_API uint32_t releasePlugin(PluginIdx_t idx);

extern IPLUGIN_DLL_API uint32_t registerInputPort(
    PluginIdx_t idx, size_t size, char name[IPLUGIN_PORT_NAME_SIZE]);

extern IPLUGIN_DLL_API uint32_t registerOuputPort(
    PluginIdx_t idx, size_t size, char name[IPLUGIN_PORT_NAME_SIZE]);


extern IPLUGIN_DLL_API uint32_t init(PluginIdx_t idx, const char *filename);
extern IPLUGIN_DLL_API uint32_t
run(PluginIdx_t idx, const void *pInput, void *const pOutput);
extern IPLUGIN_DLL_API uint32_t
finalize(PluginIdx_t idx, const void *pInput, void *const pOutput);
}


#endif /* IPLUGINIF_H */
