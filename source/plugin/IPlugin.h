#ifndef IPLUGIN_H
#define IPLUGIN_H

#include <cstdint>

class IPlugin
{
    public:
    IPlugin(){};
    virtual ~IPlugin(){};

    virtual uint32_t init(const char *filename) = 0;
    virtual uint32_t run(const void *pInput, void *const pOutput) = 0;
    virtual uint32_t finalize(const void *pInput, void *const pOutput) = 0;
};


#endif /* IPLUGIN_H */
