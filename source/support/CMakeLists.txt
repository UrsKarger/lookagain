cmake_minimum_required(VERSION 3.6)

add_library(support STATIC "")

set(LOCAL_SOURCE
    ${CMAKE_CURRENT_LIST_DIR}/support.cpp
    )
    
set(LOCAL_HEADER
    )

set(PUBLIC_HEADER
    ${CMAKE_CURRENT_LIST_DIR}/support.h
    )

set(LOCAL_INCLUDE_DIRS
    ${CMAKE_CURRENT_LIST_DIR}
    )

target_compile_options(support
    PRIVATE /W4)
    
target_sources(support
    PRIVATE
        ${LOCAL_SOURCE}
    PRIVATE
        ${LOCAL_HEADER}
    PUBLIC
        ${PUBLIC_HEADER}
)
   
target_include_directories(support
    PUBLIC
        ${LOCAL_INCLUDE_DIRS}
)

set_target_properties(support
    PROPERTIES
        CXX_STANDARD 14
        CXX_STANDARD_REQUIRED ON
)

#get_target_property(targetSourceFiles brake SOURCES)
#source_group(TREE ${CMAKE_CURRENT_LIST_DIR}
#    PREFIX "brake"
#    FILES ${targetSourceFiles}
#    )
    