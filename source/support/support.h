#ifndef SUPPORT_H
#define SUPORT_H

#include <float.h>
#include <array>
#include <algorithm>
#include <iterator>
#include <utility>


template <typename T, std::size_t noOfPts> class tCharacteristics
{
    std::array<std::pair<T,T>, noOfPts> interpolationPoints;

public:
    tCharacteristics(
        std::array<T, noOfPts> xPts,
        std::array<T, noOfPts> yPts)
    {
    /* TODO: make this a compile time check s*/
    if ((xPts.size() != yPts.size()) || (xPts.size() < noOfPts))
    {
        return;
    }

    int pos = 0;
    std::for_each(std::begin(
        interpolationPoints),
        std::end(interpolationPoints),
        [&xPts, &yPts, &pos](std::pair<T,T>& elem) { elem.first = xPts[pos]; elem.second = yPts[pos]; ++pos; });
    }

    T interpolate(T x);
};

template <class T, std::size_t noOfPts>
T tCharacteristics<T, noOfPts>::interpolate(T x)
{
    T result;

    if (x <= interpolationPoints[0].first)
    {
        result = interpolationPoints[0].second;
    }
    else if (x >= interpolationPoints[interpolationPoints.size() - 1].first)
    {
        result = interpolationPoints[interpolationPoints.size() - 1].second;
    }
    else
    {
        auto rStart = ++(std::rbegin(interpolationPoints));
        auto minPos = std::find_if(
            rStart,
            std::rend(interpolationPoints),
            [x](const std::pair<T,T>& t) {return (t.first <= x); }
        );

        auto x0 = minPos->first;
        auto y0 = minPos->second;
        auto x1 = (--minPos)->first;
        auto y1 = (minPos)->second;
        result = (y0 * (x1 - x) + y1 * (x - x0)) / (x1 - x0);

    }

    return result;
}

// Precise method, which guarantees v = v1 when t = 1.
float lerp(float v0, float v1, float t) {
    return (1 - t) * v0 + t * v1;
}


#endif /* SUPORT_H */