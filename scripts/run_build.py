#!/usr/bin/env python3
import os
import shutil
import subprocess
import argparse
import logging
import configparser

def main():
    parser = argparse.ArgumentParser();
    parser.add_argument("--log", dest='log_file', default='', action="store",
                        help="log filename, if none given logging is only done on screen");
    parser.add_argument("--wsp_dir", dest='wsp_dir', action="store",
                        help="project workspace directory name");
    parser.add_argument("--cfg", dest='cfg_file', default='cmake_cfg.ini', action="store",
                        help="configuration file name");
    parser.add_argument("--target", dest='build_target', default='TestLib', action="store",
                        help="build target");
    parser.add_argument("--build", dest='build_cfg', default='Release', action="store",
                        help="build configuration, e.g. Release");
    parser.add_argument("--root", dest='prj_root', action="store",
                        help="project root directory");
    args = parser.parse_args();

    if (args.log_file != ''):
        logging.basicConfig(filename=args.log_file, level=logging.INFO)
    else:
        logging.basicConfig(level=logging.INFO)

    ''' read cfg file and replace values with cmd line arguments if necessary '''
    config = configparser.ConfigParser()
    config.read(args.cfg_file)
    if ((args.wsp_dir != None)
        and (args.wsp_dir != '')):
        config['PROJECT']['prj_workspace_dir'] = args.wsp_dir
        
    logging.debug("test if cmake is available")
    cmake_cmd = os.path.join(config['CMAKE']['cmake_inst_dir'],
        config['CMAKE']['cmake_bin_dir'],
        config['CMAKE']['cmake_cmd'])
    logging.debug("trying to run cmake from \n %s", cmake_cmd)
    
    if (os.path.exists(cmake_cmd)):
        try:
            subprocess.check_output([cmake_cmd, "--help"])
        except subprocess.CalledProcessError as e:
            logging.error("cmake not found.\n %s \nreturned:\n %s ", e.cmd, e.output)
            exit()
    else:
        logging.error("cmake not found here:\n %s", cmake_cmd)
        exit()
        
    ''' check and create the workspace directory '''
    workspace_dir = os.path.join(os.path.abspath(args.prj_root), config['PROJECT']['prj_workspace_dir'] )

    if (not os.path.isdir(workspace_dir)):
        logging.error("no or wrong project workspace directory given: \n%s", args.prj_root)
        logging.error("maybe worspace generation script must be run first")
        exit()
    
    ''' run the cmake project command '''
    complete_command = cmake_cmd + ' ' \
                + '--build "' + workspace_dir + '"' + ' ' \
                + '--target ' + args.build_target + ' ' \
                + '--config ' + args.build_cfg
    try:
        logging.info("Running cmake command\n %s \n...", complete_command)
        subprocess.check_output(complete_command)

    except subprocess.CalledProcessError as e:
        logging.error("target not build.\n %s \nreturned:\n %s ", e.cmd, e.output)
        exit()
    
    logging.info("project build:\n %s", workspace_dir)

if __name__ == "__main__":
    main()