#!/usr/bin/env py -3
import os
import shutil
import subprocess
import argparse
import logging
import configparser

def main():
    parser = argparse.ArgumentParser();
    parser.add_argument("--log", dest='log_file', default='', action="store",
                        help="log filename, if none given logging is only done on screen");
    parser.add_argument("--name", dest='prj_name', action="store",
                        help="project name");
    parser.add_argument("--wsp_dir", dest='wsp_dir', action="store",
                        help="project workspace directory name");
    parser.add_argument("--cfg", dest='cfg_file', default='cmake_cfg.ini', action="store",
                        help="configuration file name");
    parser.add_argument("--root", dest='prj_root', action="store",
                        help="project root directory");
    parser.add_argument("--delete", dest='delete_workspace', action="store_true",
                        help="delete workspace");
    parser.add_argument("--delete_cache", dest='delete_cache', action="store_true",
                        help="delete cmake cache file if it already exists");
    args = parser.parse_args();

    if (args.log_file != ''):
        logging.basicConfig(filename=args.log_file, level=logging.INFO)
    else:
        logging.basicConfig(level=logging.INFO)

    ''' read cfg file and replace values with cmd line arguments if necessary '''
    config = configparser.ConfigParser()
    config.read(args.cfg_file)
    wsp_dir = config['PROJECT'].get('prj_workspace_dir', '')
    if ((args.prj_name != None)
        and (args.prj_name != '')):
        config['PROJECT']['prj_name'] = args.prj_name
    if ((args.wsp_dir != None)
        and (args.wsp_dir != '')):
        wsp_dir = args.wsp_dir

    logging.info("generating project: %s", config['PROJECT']['prj_name'])
    
    logging.debug("testing if cmake is available")
    cmake_cmd = os.path.join(config['CMAKE']['cmake_inst_dir'],
        config['CMAKE']['cmake_bin_dir'],
        config['CMAKE']['cmake_cmd'])
    logging.debug("trying to run cmake from \n %s", cmake_cmd)
    
    if (os.path.exists(cmake_cmd)):
        try:
            subprocess.check_output([cmake_cmd, "--help"])
        except subprocess.CalledProcessError as e:
            logging.error("cmake not found.\n %s \nreturned:\n %s ", e.cmd, e.output)
            exit()
    else:
        logging.error("cmake not found here:\n %s", cmake_cmd)
        exit()
        
    ''' check and create the workspace directory '''
    workspace_dir = os.path.join(os.path.abspath(args.prj_root), wsp_dir )
    source_dir = os.path.join(os.path.abspath(args.prj_root), config['PROJECT']['prj_source_dir'] )
    cmake_prj_dir = os.path.join(os.path.abspath(args.prj_root), config['PROJECT']['prj_cmake_dir'] )

    update_only = False
    
    if ( os.path.exists(workspace_dir) 
        and (os.path.samefile(os.path.abspath(args.prj_root), workspace_dir)) ):
        logging.error('root and workspace directory are the same!\n check options "--root", "--wsp_dir" or "[PROJECT][prj_workspace_dir] in the cfg file')
        exit()
    
    if ((args.prj_root != None)
        and (args.prj_root != '')
        and (os.path.isdir(args.prj_root))):
        create_new_wsp = True
        if (os.path.isdir(workspace_dir)):
            create_new_wsp = False
            logging.info("workspace directory exits:\n %s", workspace_dir)
            if (args.delete_workspace):
                logging.info("deleting existing workspace")
                shutil.rmtree(workspace_dir)
                create_new_wsp = True
            elif (args.delete_cache):
                cmakeCache = os.path.join(workspace_dir, "CMakeCache.txt")
                if (os.path.exists(cmakeCache)):
                    logging.info("deleting cmake cache file:\n %s", cmakeCache)
                    os.remove(cmakeCache)
                    update_only = False
            else:
                update_only = True;
              
        if (True == create_new_wsp):
            try:
                os.makedirs(workspace_dir)
                update_only = False
            except OSError as e:
                if e.errno != errno.EEXIST:
                    logging.error("could not create the workspace directory:\n %s", workspace_dir)
                    exit()
    else:
        logging.error("no or wrong project directory given: \n%s", args.prj_root)
        exit()
    
    ''' run the cmake project command '''
    script_wd = os.getcwd()
    os.chdir(workspace_dir)
    if (update_only == False):
        complete_command = cmake_cmd + ' ' \
                        + '-G "' + config['CMAKE']['generator'] + ' ' + config['CMAKE']['architecture'] + '" ' \
                        + '-DMY_PROJECT_NAME=' + config['PROJECT']['prj_name'] + ' ' \
                        + '-DMY_NAME_OF_EXE=' + config['PROJECT']['prj_exe_name'] + ' ' \
                        + '-DCMAKE_EXPORT_COMPILE_COMMANDS=ON' + ' ' \
                        + '-DMY_CMAKE_INCL_PATH=' + cmake_prj_dir + ' ' \
                        + '-Wdeprecated' + ' ' \
                        + '"' + source_dir + '"'
    else:
        complete_command = cmake_cmd + ' "' + workspace_dir + '"'
    try:
        logging.info("Running cmake command\n %s \n...", complete_command)
        subprocess.check_output(complete_command)

    except subprocess.CalledProcessError as e:
        logging.error("workspace not generated.\n %s \nreturned:\n %s ", e.cmd, e.output)
        exit()
    
    os.chdir(script_wd)
    logging.info("project generated:\n %s", workspace_dir)

if __name__ == "__main__":
    main()