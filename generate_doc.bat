@echo off

set MY_PROJECT_NAME=TestPsdEhr
set MY_NAME_OF_EXE=TestPsd
set MY_SOURCE_PATH=source
set MY_BUILD_DIR=workspace
set MY_CMAKE_INCL_DIR=cmake

set CMD_DIR=c:\Program Files\CMake
set CMAKE_CMD="%CMD_DIR%\bin\cmake.exe"

set MY_GENERATOR=Visual Studio 15 2017
set MY_ARCH=Win64

set MY_WORKSPACE_DIR="%~dp0%MY_BUILD_DIR%"
set MY_SOURCE_DIR="%~dp0%MY_SOURCE_PATH%"
set MY_CMAKE_INCL_PATH="%~dp0%MY_CMAKE_INCL_DIR%"

%CMAKE_CMD% --help > NUL 2>&1
if ERRORLEVEL 9009 (
    echo CMAKE not found - looked here %CMAKE_CMD%
    GOTO EOF
)

goto :run_cmake_cmd

choice /C YN /T 3 /D N /M "change project name (default:%MY_PROJECT_NAME%)"
IF ERRORLEVEL 0 GOTO run_cmake_cmd
IF ERRORLEVEL 1 GOTO change_project_name
IF ERRORLEVEL 2 GOTO run_cmake_cmd

:change_project_name
set MY_PROJECT_NAME_INP=
set /p MY_PROJECT_NAME_INP="Project Name [Default %MY_PROJECT_NAME%] :"
set MY_PROJECT_NAME_INP=%MY_PROJECT_NAME_INP:"=%
if NOT "%MY_PROJECT_NAME_INP%" == "" (
    set MY_PROJECT_NAME=%MY_PROJECT_NAME_INP%
)

:run_cmake_cmd
if NOT EXIST %MY_WORKSPACE_DIR% (
    echo ::: 
    echo : could not find workspace
    echo : %MY_WORKSPACE_DIR%
    echo : run run_cmake.bat first
    echo ::: 
    goto :EOF
)    

echo ::: 
echo : running CMake in:
echo : %MY_WORKSPACE_DIR%
echo ::: 
%CMAKE_CMD% --build "%MY_WORKSPACE_DIR%" ^
    --target doc

:EOF
pause