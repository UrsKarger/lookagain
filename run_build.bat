@echo off
set PRJ_ROOT=%~dp0
echo project root: %PRJ_ROOT%
py .\scripts\run_build.py --cfg .\scripts\cmake_cfg.ini --root %PRJ_ROOT% --build Release
py .\scripts\run_build.py --cfg .\scripts\cmake_cfg.ini --root %PRJ_ROOT% --build Debug

