cmake_minimum_required(VERSION 3.6)

find_path(PATH_TO_FMT "CMakeLists.txt"
    HINTS 
        "../../fmt/fmt-5.2.1"
)

add_library(fmt INTERFACE)
target_include_directories(fmt
    INTERFACE
        ${PATH_TO_FMT}/include
)
