cmake_minimum_required(VERSION 3.6)

find_path(PATH_TO_JSONCPP "json.hpp"
    HINTS "../../extern/json-develop/single_include/nlohmann"
)

add_library(JsonCpp INTERFACE)
target_include_directories(JsonCpp
    INTERFACE
        ${PATH_TO_JSONCPP}
)


#include(FindPackageHandleStandardArgs)
#find_package_handle_standard_args(Catch2 DEFAULT_MSG PATH_TO_CATCH2)

#mark_as_advanced(LIBXML2_INCLUDE_DIR)

#set(LIBXML2_INCLUDE_DIRS ${LIBXML2_INCLUDE_DIR} )

