cmake_minimum_required(VERSION 3.6)

find_path(PATH_TO_CATCH2 "catch.hpp"
    HINTS
        "../../extern/catch2"
        "../../../extern/catch2"
        "../extern/catch2"
)

message(STATUS "path to catch2: ${PATH_TO_CATCH2}")

add_library(Catch2 INTERFACE)
target_include_directories(Catch2
    INTERFACE
        ${PATH_TO_CATCH2}
)


#include(FindPackageHandleStandardArgs)
#find_package_handle_standard_args(Catch2 DEFAULT_MSG PATH_TO_CATCH2)

#mark_as_advanced(LIBXML2_INCLUDE_DIR)

#set(LIBXML2_INCLUDE_DIRS ${LIBXML2_INCLUDE_DIR} )

