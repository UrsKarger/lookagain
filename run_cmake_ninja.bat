@echo off

set PATH=%PATH%;C:\Users\urs\source\repos\plotwidget

set MY_NAME_OF_EXE=PlotTest
set MY_PROJECT_NAME=plotW
set MY_SOURCE_PATH=source
set MY_CMAKE_INCL_DIR=cmake

set CMD_DIR=c:\Program Files\CMake
set CMAKE_CMD="%CMD_DIR%\bin\cmake.exe"

set MY_GENERATOR=Visual Studio 15 2017
set MY_ARCH=Win64

REM set MY_GENERATOR=MinGW Makefiles
REM set MY_GENERATOR=Ninja



REM path to the top level CMakeList.txt file - leave empty if its in 

set MY_OPTION_QT=USE_QT


set MY_WORKSPACE_DIR="%~dp0%MY_BUILD_DIR%"
set MY_SOURCE_DIR="%~dp0%MY_SOURCE_PATH%"
set MY_CMAKE_INCL_PATH="%~dp0%MY_CMAKE_INCL_DIR%"

%CMAKE_CMD% --help > NUL 2>&1
if ERRORLEVEL 9009 (
    echo CMAKE not found - looked here %CMAKE_CMD%
    GOTO EOF
)

choice /C YN /T 3 /D N /M "change project name (default:%MY_PROJECT_NAME%)"
IF ERRORLEVEL 0 GOTO run_cmake_cmd
IF ERRORLEVEL 1 GOTO change_project_name
IF ERRORLEVEL 2 GOTO run_cmake_cmd

:change_project_name
set MY_PROJECT_NAME_INP=
set /p MY_PROJECT_NAME_INP="Project Name [Default %MY_PROJECT_NAME%] :"
set MY_PROJECT_NAME_INP=%MY_PROJECT_NAME_INP:"=%
if NOT "%MY_PROJECT_NAME_INP%" == "" (
    set MY_PROJECT_NAME=%MY_PROJECT_NAME_INP%
)

:run_cmake_cmd
if EXIST %MY_WORKSPACE_DIR% (
    echo ::: 
    echo : deleting old workspace directory
    echo : %MY_WORKSPACE_DIR%
    echo ::: 
    rmdir %MY_WORKSPACE_DIR% /q /s
)    
mkdir %MY_WORKSPACE_DIR%

pushd %MY_WORKSPACE_DIR%
echo ::: 
echo : running CMake in:
echo : %MY_WORKSPACE_DIR%
echo ::: 
%CMAKE_CMD% -G "%MY_GENERATOR% %MY_ARCH%" ^
    -DMY_PROJECT_NAME=%MY_PROJECT_NAME% ^
    -DMY_NAME_OF_EXE=%MY_NAME_OF_EXE% ^
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ^
    -DMY_CMAKE_INCL_PATH=%MY_CMAKE_INCL_PATH% ^
    -Wdeprecated ^
    %MY_SOURCE_DIR%
popd

REM remove quotes
REM set widget="a very useful item"
REM set widget
REM set widget=%widget:"=%
REM set widget
:EOF
pause